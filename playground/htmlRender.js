import React from 'react';
import {View, Text, ScrollView, Dimensions, StyleSheet} from 'react-native';

import HTML from 'react-native-render-html';

const CUSTOM_STYLES = {
};
const CUSTOM_RENDERERS = {
    bluecircle: () => <View style={{ width: 20, height: 20, borderRadius: 10, backgroundColor: 'blue' }} />
};
const CUSTOM_CLASSESSTYLES = {
    'last-paragraph': { textAlign: 'right', color: 'teal', fontWeight: '800' }
}
const DEFAULT_PROPS = {
    htmlStyles: CUSTOM_STYLES,
    renderers: CUSTOM_RENDERERS,
    classesStyles: CUSTOM_CLASSESSTYLES
};

class Item extends React.Component {
    

    
    render () {
    console.log(this.props.navigation);
        
    const htmlContent = `
    <h1>This HTML snippet is now rendered with native components !</h1>
    <h2>Enjoy a webview-free and blazing fast application</h2>
    <img src="https://i.imgur.com/dHLmxfO.jpg?2" />
    <em style="textAlign: center;">Look at how happy this native cat is</em>
    <i>Here, we have a style set on the "i" tag with the "tagsStyles" prop.</i>
    <p class="last-paragraph">Finally, this paragraph is styled through the classesStyles prop</p>
    <bluecircle></bluecircle>
    `;
    
    const renderers = {
        tagsStyles: () => { <i textAlign='center' fontStyle= 'italic' color='grey' /> },

    }
    var tagsStyles = { i: { textAlign: 'center', fontStyle: 'italic', color: 'grey' } }
    var classesStyles =  { 'last-paragraph': { textAlign: 'right', color: 'teal', fontWeight: '800' } }

    return (
            <ScrollView style={{ flex: 1 }}>
                <HTML 
                html={htmlContent} 
                imagesMaxWidth={Dimensions.get('window').width}
                {...DEFAULT_PROPS}

                />
            </ScrollView>
    )
    }
    
}

export default Item