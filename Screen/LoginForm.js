import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput, TouchableOpacity, StatusBar } from 'react-native';

import firebaseApp from '../src/db';
import firebase from 'react-native-firebase';

import Loader from '../Components/Loader';

export default class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
        username: "",
        passwd: "",
        loading: false
    };
  }

  onChangeText (sender, text) {
    if (sender === this.usernameInput) {
        this.setState({
            username: text
        })
    }
    else {
        this.setState({
            passwd: text
        })
    }
  }
  
  render() {
      
    buttonWasPressed = (screen, job) => {
        this.setState({
            loading: true
        })
        const navigateScreen = this.props.onPress
        
        // navigateScreen(screen)
        if (job === 'loginJob') {
            
            // Sign in existing user
            firebaseApp.auth().signInWithEmailAndPassword(this.state.username, this.state.passwd)
            .then(() => {
                navigateScreen(screen)
                this.setState({
                    loading: false
                })

            })
            .catch(function(err) {
                // Handle errors
                alert('Error ', err)
            });
        }
        else {
            // Register
            firebaseApp.auth().createUserWithEmailAndPassword(this.state.username, this.state.passwd)
            .then(() => {
                alert('Successfully!')
                this.setState({
                    username: '',
                    passwd: '',
                    loading: false
                })
              //  navigateScreen('LoginScreen')
            })
            .catch(function (err) {
                
                // Handle errors
                alert("minimum of 6 characters!")

            });
        }
        setTimeout(() => {
            this.setState({
                loading: false
            })
        }, 2000);
        
    }

    
    return (
      <View style={styles.container}>
      <StatusBar barStyle="light-content"/>
      <Loader
        loading={this.state.loading} />
            <TextInput
            placeholder="username or email"
            placeholderTextColor={"rgba(255,255,255,0.4)"}
            autoCorrect={false}
            autoCapitalize={false}
            returnKeyType="next"
            onSubmitEditing={() => this.passwordInput.focus()}
            ref = {(input) => this.usernameInput = input}
            style={styles.input}

            onChangeText={ (text) => {
                this.onChangeText(this.usernameInput, text)
                }
                }
            ></TextInput>

            <TextInput
            placeholder="password"
            placeholderTextColor={"rgba(255,255,255,0.4)"}
            autoCorrect={false}
            autoCapitalize={false}
            returnKeyType="go"
            ref = {(input) => this.passwordInput = input}
            style={styles.input}

            onChangeText={
                (text) => {
                    this.onChangeText(this.passwordInput, text)
                }
            }
            ></TextInput>

            <TouchableOpacity style={styles.buttonContainer}
            onPress={() => {
                buttonWasPressed('HomeScreen', 'loginJob')
            }}
            >
                <Text style={styles.buttonText}>Login</Text>
            </TouchableOpacity>

            <Text style={styles.title}>or</Text>
            <TouchableOpacity style={styles.buttonContainer}
            onPress={() => {
                buttonWasPressed('RegistationScreen', 'registerJob')
            }}
            >
                <Text style={styles.buttonText}>Register</Text>
            </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        padding: 20,
        
    },
    input: {
        height: 40,
        backgroundColor: 'rgba(255,255,255,0.2)',
        color: '#fff',
        paddingHorizontal: 20,
        marginBottom: 10,
    },

    title: {
        textAlign: 'center',
        color: 'rgba(255,255,255,0.5)',
        marginBottom: 10
    },

    buttonContainer: {
        backgroundColor: "#2980b9",
        paddingVertical: 10,
        marginBottom: 10
    },

    buttonText: {
        textAlign: 'center',
        fontWeight:'700',
        color:'#fff'
    }
})