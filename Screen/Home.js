import React, {Component} from 'react';
import {View, Text, ScrollView, TouchableOpacity, Dimensions, StyleSheet} from 'react-native';

import { SearchBar, Button } from 'react-native-elements';

import Item from './Item';

import firebaseApp from '../src/db';

const firestore = firebaseApp.firestore();

const screenSize = {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
}

// let db = firebaseApp.database()

// let connectedRef = db.ref('.info/connected');

// connectedRef.on('value', (snapshot) => {
//     let isOnline = snapshot.val();
//     if (isOnline) {
//         console.log("Yay! You're online")
//     }
//     else {
//         console.log("Opp! You're offline")
//     }
// })

class Suggestions extends Component {
    constructor(props) {
        super(props)
        this.state = {
            item: props.item
        }
    }
    
    render() {
        handle = () => {
            const {item, onPress} = this.props
            onPress(item.title)
        }
        
        
        return (
            <TouchableOpacity style={{margin:10}} onPress={handle}>
                <Text>{this.props.item.title}</Text>
            </TouchableOpacity>
        )
        
    }

}

class Search extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            results: [],
            text: ""
        }
        this.ref = firestore.collection('dict');
    }
      
    componentWillUnmount() {
        // this.unsubscribe()
    }
    onChangeText (text) {
        if (!text) {
            this.setState ({
                text: "",
                results: []
            }) 
            return
        }
        this.setState({
            text: text
        })

        this.unsubscribe = this.ref.orderBy('title').startAt(text).limit(10).endAt(text  + '\uf8ff').onSnapshot(this.onCollectionUpdate)
    }

    onCollectionUpdate = (querySnapshot) => {
        var items = []
        querySnapshot.forEach((doc) => {
            let item = doc.data()
            items.push(item)
        })
        this.setState({
            results: items
        }); 
    }

    renderSuggestion() {
        // console.log(this.state.results);
        let arr = this.state.results
        return arr.map((item) => {
            return  <Suggestions item={item} onPress={this.props.onPress}></Suggestions>
            // return <Text> adasd</Text>
        })
        

    }

    render() {

        
        return (
            <View>
                <SearchBar
                    showLoading
                    platform="ios"
                    lightTheme
                    autoCapitalize={false}
                    autoCorrect={false}
                    cancelButtonTitle="Cancel"
                    onChangeText = {(text, text1) => {
                        console.log(`event ${text1}`)
                        this.onChangeText(text)}}
                    placeholder='Type Here...'
                />
                
                <ScrollView>
                    {this.renderSuggestion()}
                </ScrollView>
            </View>
        )
    }
}

class Home extends React.Component {
    constructor(props) {
        super(props)
        
    }
    componentWillMount() {
        console.log('Home will mount')
    }

    componentDidUpdate() {
        console.log('Home did update')
    }

    componentWillUnmount() {
        
    }

    navigateScreen(title) {
        const { navigate } = this.props.navigation
        navigate('ItemScreen', {title: title, name: title})
    }

    render () {
        const { navigate } = this.props.navigation
    
        /* jshint ignore:start */
    return (
        <ScrollView style={{flex: 1}}>
            <Search onPress={this.navigateScreen.bind(this)}></Search>
            <View style={{flex: 2, alignItems:'center',
            //  backgroundColor:'black'
             }}>
            
                <Button
                    rounded = 'true'
                    title='Globse'
                    backgroundColor='green'
                    style = {styles.button} 
                    />
                <Button
                    rounded = 'true'
                    title='Some thing Diff'
                    backgroundColor='violet'
                    style = {styles.button}
                    />
            </View>
        </ScrollView>

    )
    /* jshint ignore:end */
    }
}

const styles = StyleSheet.create({
    button: {
        padding: 10,
        width: screenSize.width - 40
    }
})

export default Home