import React from 'react';
import {View, Text, ScrollView, Dimensions, StyleSheet} from 'react-native';

import HTML from 'react-native-render-html';

import firebaseApp from '../src/db';

const firestore = firebaseApp.firestore();

const CUSTOM_STYLES = {
};
const CUSTOM_RENDERERS = {
};
const CUSTOM_CLASSESSTYLES = {
    'last-paragraph': { textAlign: 'right', color: 'teal', fontWeight: '800' },
    'fi': {color: '#0061ff', fontWeight: 'bold'},
    'thn': {color: '#b51a00', fontStyle: 'italic'},
    'bk': {color: '#006d8f', fontWeight: 'bold'},
    'head': { color: '#E66C2C', fontFamily: 'Times New Roman', fontWeight: 'bold', fontSize: 22},
    'maucau': { color: 'green', fontStyle: 'italic', fontWeight: 'bold'},
    'lt': {color: '#4f7a28', fontWeight: 'bold'},
    'm': {color: '#9a244f', fontFamily: "Times New Roman", fontWeight: 'bold'}
}

const CUSTOM_TAGSSTYLES = {
    font: {color: 'red'},
}

const DEFAULT_PROPS = {
    classesStyles: CUSTOM_CLASSESSTYLES,
    // renderers: CUSTOM_RENDERERS,
    listsPrefixesRenderers: {
        ul: (htmlAttribs, children, convertedCSSStyles, passProps) => {
            return (
                <Text style={{ color: 'blue', fontSize: 16 }}></Text>
            );
        }
    }
};

class Item extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            items: {
                title: "",
                content: "",
            }
        }
        this.ref = firestore.collection('dict');
    }

    componentWillMount() {
        
        let title = this.props.navigation.state.params.title

        this.unsubscribe = this.ref.where('title', '=', title).onSnapshot(this.onCollectionUpdate)
    }
    
    componentWillUnmount() {
        this.unsubscribe();
    }

    onCollectionUpdate = (querySnapshot) => {
        querySnapshot.forEach((doc) => {
            // console.log(doc);
            let item = doc.data()
            this.setState({items: item});
            this.props.navigation.setParams({
                items: {
                    ...item,
                    doc: doc
                }
            });
        });
    }

    render () {
        let arr = this.props
        // console.log(this.props)
        const htmlContent = ""

        return (
                <ScrollView style={{ flex: 1 , margin: 10}}>
                    <HTML html={`${this.state.items.content}`}
                        {...DEFAULT_PROPS}
                    />
                </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    a: {
      fontWeight: '300',
      color: '#FF3366',
    },
    font: {color: 'red'},
    fi: {color: '#0061ff', fontWeight: 'bold'},
  });

export default Item

