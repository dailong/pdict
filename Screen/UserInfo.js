import React, { Component } from 'react';
import { View, Text, StyleSheet, StatusBar } from 'react-native';

import { ListItem, Icon } from 'react-native-elements';

import firebaseApp from '../src/db';

export default class UserInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const { navigate } = this.props.navigation

    return (
      <View style={styles.container}>
      <StatusBar barStyle='light-content'/>
        <View>
          <ListItem 
          title="Sign Out"
          titleStyle={{ color: '#3498db', fontWeight: '700' }}
          chevronColor="white"
          onPress = {() => {
            firebaseApp.auth().signOut()
            .then(() => {
              navigate('LoginScreen')
            })
                        .catch(function (err) {
                        // Handle errors
                        alert('Cant log out')
                        })
          }}
          ></ListItem>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: '#FFF',
  },

  section: {
    marginTop: 20,
  }
})

