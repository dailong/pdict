/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Navigator, TouchableOpacity, Image} from 'react-native';

import { createStackNavigator, Header } from 'react-navigation';

import firebaseApp from './src/db';
import firebase from 'react-native-firebase';


import LoginScreen from './Screen/Login';
import Registation from './Screen/Registation';
import UserInfo from './Screen/UserInfo';
import Home from './Screen/Home';
import Item from './Screen/Item';
import EditContent from './Screen/EditContent';

const defautlAva = require('./Resources/default-user.png')

const navigationHeight = Header.HEIGHT;

const styles = StyleSheet.create({
  buttonContainer: {
      marginLeft: 15,
      marginRight: 15,
      height: navigationHeight - 20,
      // width: navigationHeight - 20,
      // backgroundColor: '#000',
      alignItems:'center',
      justifyContent:'center',
  },
  buttonCircle: {
    borderWidth:1,
    borderColor:'rgba(0,0,0,0.5)',
    alignItems:'center',
    justifyContent:'center',
    height: navigationHeight - 30,
      width: navigationHeight - 30,
    backgroundColor:'#fff',
    borderRadius:navigationHeight - 30,
    // marginRight: 10
  }, 
  buttonSystem: {
    alignItems:'center',
    justifyContent:'center',
    height: navigationHeight - 30,
      // width: navigationHeight - 30,
    // backgroundColor:'#fff',
  },
  textButton: {
    color: '#fff',
    fontWeight: 'bold'
  },
  image: {
    height: navigationHeight - 35,
    width: navigationHeight - 35,
  }
});

const AppNavigation = createStackNavigator({
  HomeScreen: { 
    screen: Home,

    navigationOptions: ({navigation}) => ({
      headerTitle: 'Home',
      headerMode: 'screen',
      headerLeft: null,
      headerStyle: {
        backgroundColor: '#3498db',
      },
      headerTintColor: '#FFF',
      gesturesEnabled: false,
      
      headerRight: (
        <View style={styles.buttonContainer} >
        <TouchableOpacity
          style={styles.buttonCircle}
          onPress = {() => {
              navigation.navigate('UserInfoScreen')
            }}
          >
          <Image style= {styles.image}
          source={defautlAva} />
         </TouchableOpacity>
         </View>
      ),
  })
},

  ItemScreen: { 
    screen: Item,
    navigationOptions: ({ navigation}) => ({
      headerTitle: `${navigation.state.params.name}`,
      headerMode: 'screen',
      headerStyle: {
        backgroundColor: '#3498db',
      },
      headerTintColor: '#FFF', 
      headerRight: (
        <View style={styles.buttonContainer} >
        <TouchableOpacity
          style={styles.buttonSystem}

          onPress = {() => {
              navigation.navigate('EditContentScreen', navigation.state.params.items, navigation.state.params.doc)
            }}
          >
          <Text style={styles.textButton}>Edit</Text>
         </TouchableOpacity>
         </View>
      ),
    })
   },
   LoginScreen: { 
    screen: LoginScreen,

    navigationOptions: {
      header: null,
      gesturesEnabled: false
      
  }},

  EditContentScreen: { 
    screen: EditContent,

    navigationOptions: ({navigation}) => ({
      headerTitle: `Editing`,
      headerStyle: {
        backgroundColor: '#3498db',
      },
      headerTintColor: '#FFF',

      // Turn off gesture
      gesturesEnabled: false,

      headerRight: (
        <View style={styles.buttonContainer} >
        <TouchableOpacity
          style={styles.b}
          onPress = {() => {
            navigation.state.params.saveChanged()
            }}
          >
          <Text style={styles.textButton}>Save</Text>
         </TouchableOpacity>
         </View>
      ),

      headerLeft: (
        <View style={styles.buttonContainer} >
        <TouchableOpacity
          style={styles.buttonSystem}
          onPress = {() => {
              navigation.goBack()
            }}
          >
          <Text style={styles.textButton}>Cancel</Text>
         </TouchableOpacity>
         </View>
      ),
    })   
  },

    RegistationScreen: { 
      screen: Registation,

    navigationOptions: {
      headerTitle: 'Registation',
      
  }},
  UserInfoScreen: { 
    screen: UserInfo,

    navigationOptions: {
      headerTitle: 'Account',
      headerStyle: {
        backgroundColor: '#3498db',
      },
      headerTintColor: '#FFF'
  }},
}, 
{
  initialRouteName: 'LoginScreen',
  // headerMode: 'none'
});


export default class App extends Component {
  render() {
    return (
      <AppNavigation />
    );
  }
}



